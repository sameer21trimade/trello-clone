import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getCards, addCard, updateCard, deleteCard } from '../actions/postActions'

class Card extends React.Component {
    constructor() {
        super()
        this.state =
        {
            newCard: false,
            userInput: ''
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleEdit = this.handleEdit.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    componentDidMount() {
        let currentBoardId = this.props.boardId
        this.props.getCards(currentBoardId)
    }

    handleClick = () => {
        this.setState({ newCard: true })
    }

    handleChange = (event) => {
        this.setState({ userInput: event.target.value })
    }

    handleSubmit(event) {
        event.preventDefault()
        let cardName = this.state.userInput
        let currentListId = this.props.id
        let currentBoardId = this.props.boardId
        this.props.addCard(cardName, currentListId, currentBoardId)
        this.setState({ newCard: false })
    }

    handleEdit = (cardId) => {
        let cardName = this.state.userInput
        this.props.updateCard(cardId, cardName)
        alert('Card edited successfully.')
    }

    handleDelete = (cardID) => {
        setTimeout(function () { alert('Card deleted successfully.') }, 2000)
        let currentBoardId = this.props.boardId
        this.props.deleteCard(cardID, currentBoardId)
    }

    render() {
        return (
            <div>
                <div>
                    {this.props.rootReducerPosts.map((index) => {
                        return (index.idList === this.props.id) ?
                            <div key={index.id} className="cardHolder" >
                                <textarea type="text" onChange={this.handleChange} className="cardItems" defaultValue={index.name}></textarea>
                                <input className="editButton" type="button" value="Edit" onClick={() => { this.handleEdit(index.id) }} />
                                <input className="deleteButton" type="button" value="Delete" onClick={() => { this.handleDelete(index.id) }} />
                            </div>
                            :
                            null
                    })
                    }
                </div>
                {this.state.newCard ? <form onSubmit={(event) => { this.handleSubmit(event) }}>
                    <div className="newCardHolder" >
                        <textarea type="text" onChange={this.handleChange} className="newCardItems" placeholder="Enter title for this card" ></textarea>
                        <input className="addCardSubmitButton" type="submit" value="Submit" /> </div>
                </form>
                    :
                    null
                }
                <input className="addCard" type="button" value="+ Add a card" onClick={this.handleClick} />
            </div>
        )
    }
}

Card.propTypes = {
    getCards: PropTypes.func.isRequired,
    addCard: PropTypes.func.isRequired,
    updateCard: PropTypes.func.isRequired,
    deleteCard: PropTypes.func.isRequired,
    rootReducerPosts: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
    rootReducerPosts: state.rootReducerPosts.getCards
})

export default connect(mapStateToProps, { getCards, addCard, updateCard, deleteCard })(Card)
