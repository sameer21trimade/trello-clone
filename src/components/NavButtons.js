import React from 'react'
import { NavLink } from "react-router-dom"
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class NavButtons extends React.Component {

    render() {
        return (
            <div>
                <h2 className="personalBoards">Personal Boards</h2>
                {this.props.rootReducerPosts.map(index => <NavLink className="boardItems" to={`/board/${index.id}`} key={index.id}  >
                    {index.name}
                </NavLink>)}
            </div>
        )
    }
}

NavButtons.propTypes = {
    rootReducerPosts: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
    rootReducerPosts: state.rootReducerPosts.dashboard
})

export default connect(mapStateToProps)(NavButtons)
