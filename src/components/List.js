import React from 'react'
import Card from './Card'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getLists, addList } from '../actions/postActions'

class List extends React.Component {
  constructor() {
    super()
    this.state =
    {
      newList: false,
      userInput: '',
    }
    this.handleClick = this.handleClick.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount() {
    let currentBoardId = this.props.id
    this.props.getLists(currentBoardId)
  }

  handleClick = () => {
    this.setState({ newList: true })
  }

  handleChange = (event) => {
    this.setState({ userInput: event.target.value })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    let listName = this.state.userInput
    let currentBoardId = this.props.id
    this.props.addList(listName, currentBoardId)
    this.setState({ newList: false })
  }

  render() {
    return (
      <div>
        <input type="button" className="addList" value="+ Add a List" onClick={this.handleClick} />
        {this.state.newList ?
          <form onSubmit={(event) => { this.handleSubmit(event) }}>
            <div className="newListBlock">
              <textarea type="text" className="newList" onChange={this.handleChange} placeholder="Enter title for this List" ></textarea>
              <input type="submit" className="addListSubmitButton" value="Submit" /> </div>
          </form>
          :
          null
        }
        <div className="List" >
          {this.props.rootReducerPosts.map(index => <div className="listItems" key={index.id}  >
            <h1>{index.name}</h1>
            <Card id={index.id} boardId={this.props.id} />
          </div>)}
        </div>
      </div>
    )
  }
}

List.propTypes = {
  getLists: PropTypes.func.isRequired,
  addList: PropTypes.func.isRequired,
  rootReducerPosts: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  rootReducerPosts: state.rootReducerPosts.getList
})

export default connect(mapStateToProps, { getLists, addList })(List)