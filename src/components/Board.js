import React from 'react'
import List from './List'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getBoard } from '../actions/postActions'
import { NavLink } from "react-router-dom"

class Board extends React.Component {

    componentDidMount() {
        let boardId = this.props.match.params.id
        this.props.getBoard(boardId)
    }

    render() {
        return (
            <div>
                <h1 className="BoardName">&#9733; {this.props.rootReducerPosts.name} &#9733; </h1>
                <List id={this.props.match.params.id} />
                <NavLink className="dashboardButton" to="/" > Go to dashboard </NavLink>
            </div>
        )
    }
}

Board.propTypes = {
    getBoard: PropTypes.func.isRequired,
    rootReducerPosts: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    rootReducerPosts: state.rootReducerPosts.getBoard
})

export default connect(mapStateToProps, { getBoard })(Board)