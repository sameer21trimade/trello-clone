import React from 'react'
import './App.css'
import Board from './components/Board'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { displayDashboard } from './actions/postActions'
import { Switch, Route } from "react-router-dom";
import NavButtons from './components/NavButtons'

class App extends React.Component {

  componentDidMount() {
    this.props.displayDashboard()
  }

  render() {
    return (
      <>
        <h1 className="Heading App">Trello</h1>
        <Switch>
          <Route path="/board/:id" component={Board} />
          <NavButtons />
        </Switch>
      </>
    )
  }
}

App.propTypes = {
  displayDashboard: PropTypes.func.isRequired
}

export default connect(null, { displayDashboard })(App)