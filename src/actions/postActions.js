import { DASHBOARD, GET_BOARD, GET_LISTS, ADD_LIST, GET_CARDS, ADD_CARD, UPDATE_CARD, DELETE_CARD } from './types'
const fetch = require('node-fetch')

export const displayDashboard = () => dispatch => {

    fetch('https://api.trello.com/1/members/sameertrimade/boards?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => dispatch({
            type: DASHBOARD,
            payload: JSON.parse(data)
        }))
        .catch(err => console.error(err))
}

export const getBoard = (boardId) => dispatch => {

    fetch('https://api.trello.com/1/boards/' + boardId + '?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => dispatch({
            type: GET_BOARD,
            payload: JSON.parse(data)
        }))
        .catch(err => console.error(err))
}

export const getLists = (currentBoardId) => dispatch => {
    fetch('https://api.trello.com/1/boards/' + currentBoardId + '/lists?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => dispatch({
            type: GET_LISTS,
            payload: JSON.parse(data)
        }))
        .catch(err => console.error(err))
}

export const addList = (listName, currentBoardId) => dispatch => {
    fetch('https://api.trello.com/1/lists?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889&name=' + listName + '&idBoard=' + currentBoardId, {
        method: 'POST'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => {
            dispatch({
                type: ADD_LIST,
                payload: JSON.parse(data)
            })
            fetch('https://api.trello.com/1/boards/' + currentBoardId + '/lists?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
                method: 'GET'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    )
                    return response.text()
                })
                .then(data => dispatch({
                    type: GET_LISTS,
                    payload: JSON.parse(data)
                }))
                .catch(err => console.error(err))
        })
        .catch(err => console.error(err))
}

export const getCards = (currentBoardId) => dispatch => {

    fetch('https://api.trello.com/1/boards/' + currentBoardId + '/cards?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => dispatch({
            type: GET_CARDS,
            payload: JSON.parse(data)
        }))
        .catch(err => console.error(err))
}

export const updateCard = (cardId, cardName) => dispatch => {

    fetch('https://api.trello.com/1/cards/' + cardId + '?key=d8e320fa8616b6342c5d13eba6266d26&name=' + cardName + '&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889&desc=', {
        method: 'PUT',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => dispatch({
            type: UPDATE_CARD,
            payload: data
        }))
        .catch(err => console.error(err))
}

export const deleteCard = (cardID, currentBoardId) => dispatch => {

    fetch('https://api.trello.com/1/cards/' + cardID + '?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
        method: 'DELETE'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => {
            dispatch({
                type: DELETE_CARD,
                payload: data
            })
            fetch('https://api.trello.com/1/boards/' + currentBoardId + '/cards?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
                method: 'GET'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    )
                    return response.text()
                })
                .then(data => dispatch({
                    type: GET_CARDS,
                    payload: JSON.parse(data)
                }))
                .catch(err => console.error(err))
        })
        .catch(err => console.error(err))
}

export const addCard = (cardName, currentListId, currentBoardId) => dispatch => {

    fetch('https://api.trello.com/1/cards?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889&name=' + cardName + '&idList=' + currentListId, {
        method: 'POST'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            )
            return response.text()
        })
        .then(data => {
            dispatch({
                type: ADD_CARD,
                payload: data
            })
            fetch('https://api.trello.com/1/boards/' + currentBoardId + '/cards?key=d8e320fa8616b6342c5d13eba6266d26&token=6c6bd7b9c940800bc8a92e61fbefa67aafb20d12f9a70be4ff067d8d03897889', {
                method: 'GET'
            })
                .then(response => {
                    console.log(
                        `Response: ${response.status} ${response.statusText}`
                    )
                    return response.text()
                })
                .then(data => dispatch({
                    type: GET_CARDS,
                    payload: JSON.parse(data)
                }))
                .catch(err => console.error(err))
        })
        .catch(err => console.error(err))
}