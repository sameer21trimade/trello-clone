import { DASHBOARD, GET_BOARD, GET_LISTS, ADD_LIST, GET_CARDS, ADD_CARD, UPDATE_CARD, DELETE_CARD } from '../actions/types'

const initialState = {
    dashboard: [],
    getBoard: {},
    getList: [],
    getCards: [],
    addCard: [],
    updateCard: [],
    deleteCard: [],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case DASHBOARD:
            return {
                ...state,
                dashboard: action.payload
            }
        case GET_BOARD:
            return {
                ...state,
                getBoard: action.payload
            }
        case GET_LISTS:
            return {
                ...state,
                getList: action.payload
            }
        case ADD_LIST:
            return {
                ...state,
                addList: action.payload
            }
        case GET_CARDS:
            return {
                ...state,
                getCards: action.payload
            }
        case ADD_CARD:
            return {
                ...state,
                addCard: action.payload
            }
        case UPDATE_CARD:
            return {
                ...state,
                updateCard: action.payload
            }
        case DELETE_CARD:
            return {
                ...state,
                deleteCard: action.payload
            }
        default:
            return state
    }
}